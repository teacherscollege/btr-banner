
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "BANINST1"."FAZABAL" ("FSYR", "FSPD", "INDEXCODE", "ACCT", "ACCTTITLE", "AVAILBAL") AS 
  SELECT     fgbopal_fsyr_code as FSYR,
           ftvfspd_fspd_code as fspd,
           ftvacci_acci_code as IndexCode,
         --  FGBOPAL_FUND_CODE AS FUND,
         --  FGBOPAL_ORGN_CODE AS ORGN,
         --  FGBOPAL_PROG_CODE AS PROG,
           FGBOPAL_ACCT_CODE AS ACCT,
           ftvacct_title     AS AcctTitle,
           NVL(decode(FTVFSPD_FSPD_CODE,
                                '00', FGBOPAL_00_adopt_bud + fgbopal_00_bud_adjt - fgbopal_00_ytd_actv - fgbopal_00_encumb - fgbopal_00_bud_rsrv,
                                '01', fgbopal_01_adopt_bud + fgbopal_01_bud_adjt - fgbopal_01_ytd_actv - fgbopal_01_encumb - fgbopal_01_bud_rsrv,
                                '02', fgbopal_02_adopt_bud + fgbopal_02_bud_adjt - fgbopal_02_ytd_actv - fgbopal_02_encumb - fgbopal_02_bud_rsrv,
                                '03', fgbopal_03_adopt_bud + fgbopal_03_bud_adjt - fgbopal_03_ytd_actv - fgbopal_03_encumb - fgbopal_03_bud_rsrv,
                                '04', fgbopal_04_adopt_bud + fgbopal_04_bud_adjt - fgbopal_04_ytd_actv - fgbopal_04_encumb - fgbopal_04_bud_rsrv,
                                '05', fgbopal_05_adopt_bud + fgbopal_05_bud_adjt - fgbopal_05_ytd_actv - fgbopal_05_encumb - fgbopal_05_bud_rsrv,
                                '06', fgbopal_06_adopt_bud + fgbopal_06_bud_adjt - fgbopal_06_ytd_actv - fgbopal_06_encumb - fgbopal_06_bud_rsrv,
                                '07', fgbopal_07_adopt_bud + fgbopal_07_bud_adjt - fgbopal_07_ytd_actv - fgbopal_07_encumb - fgbopal_07_bud_rsrv,
                                '08', fgbopal_08_adopt_bud + fgbopal_08_bud_adjt - fgbopal_08_ytd_actv - fgbopal_08_encumb - fgbopal_08_bud_rsrv,
                                '09', fgbopal_09_adopt_bud + fgbopal_09_bud_adjt - fgbopal_09_ytd_actv - fgbopal_09_encumb - fgbopal_09_bud_rsrv,
                                '10', fgbopal_10_adopt_bud + fgbopal_10_bud_adjt - fgbopal_10_ytd_actv - fgbopal_10_encumb - fgbopal_10_bud_rsrv,
                                '11', fgbopal_11_adopt_bud + fgbopal_11_bud_adjt - fgbopal_11_ytd_actv - fgbopal_11_encumb - fgbopal_11_bud_rsrv,
                                '12', fgbopal_12_adopt_bud + fgbopal_12_bud_adjt - fgbopal_12_ytd_actv - fgbopal_12_encumb - fgbopal_12_bud_rsrv,
                                '13', fgbopal_13_adopt_bud + fgbopal_13_bud_adjt - fgbopal_13_ytd_actv - fgbopal_13_encumb - fgbopal_13_bud_rsrv,
                                '14', fgbopal_14_adopt_bud + fgbopal_14_bud_adjt - fgbopal_14_ytd_actv - fgbopal_14_encumb - fgbopal_14_bud_rsrv, 0), 0) as AvailBal                              
    FROM FGBOPAL,
         FTVFSPD,
         ftvacct, 
         ftvacci
    WHERE
          FTVFSPD_COAS_CODE = '1'
    AND   FGBOPAL_FSYR_CODE = FTVFSPD_FSYR_CODE
    AND   FGBOPAL_COAS_CODE = '1'
    and   trunc(sysdate) between trunc(ftvfspd_prd_start_date) and trunc(ftvfspd_prd_end_date) 
    and  fgbopal_acct_code = ftvacct_acct_code
    and  trunc(sysdate) between trunc(ftvacct_eff_date) and trunc(ftvacct_nchg_date)
    and  fgbopal_fund_code = ftvacci_fund_code
    and  fgbopal_orgn_code = ftvacci_orgn_code
    and  fgbopal_prog_code = ftvacci_prog_code
    and  trunc(sysdate) between trunc(ftvacci_eff_date) and trunc(ftvacci_nchg_date);