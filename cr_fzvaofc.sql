
  CREATE OR REPLACE VIEW "BANINST1"."FZVAOFC" ("FZVAOFC_INDEX", "FZVAOFC_PIDM", "FZVAOFC_OFFICE", "FZVAOFC_LAST_NAME", "FZVAOFC_FIRST_NAME", "FZVAOFC_UNI") AS 
  select distinct a.fzrappr_index as fzvaofc_index, budofc.fzrappr_pidm as fzvaofc_pidm, budofc.fzrappr_sys_role as fzvaofc_office, spriden_last_name as fzvaofc_last_name, spriden_first_name as fzvaofc_first_name, spriden_id as fzvaofc_uni
from fzrappr a, (select b.fzrappr_pidm, b.FZRAPPR_SYS_ROLE, spriden_last_name, spriden_first_name, spriden_id 
                   from fzrappr b, spriden 
                   where b.fzrappr_system = 'REPORT' 
                    and b.fzrappr_sys_role = 'BUDOFC'
                    and b.fzrappr_pidm = spriden_pidm
                    and spriden_ntyp_code = 'UNI') budofc
where substr(a.fzrappr_index,1,1) in ('1','2','6','7','3','4','8','9')
union
select distinct a.fzrappr_index, grntofc.fzrappr_pidm, grntofc.fzrappr_sys_role, spriden_last_name, spriden_first_name, spriden_id 
from fzrappr a, (select b.fzrappr_pidm, b.fzrappr_sys_role, spriden_last_name, spriden_first_name, spriden_id 
                   from fzrappr b, spriden 
                   where b.fzrappr_system = 'REPORT' 
                   and b.fzrappr_sys_role = 'GRNTOFC'
                   and b.fzrappr_pidm = spriden_pidm
                   and spriden_ntyp_code = 'UNI') grntofc
where substr(a.fzrappr_index,1,1) in ('5','3','4','8','9');
