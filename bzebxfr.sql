CREATE OR REPLACE package bzebxfr as
/*******************************************************************************

   NAME:       BZEBXFR

   PURPOSE:    This package is a part of the Budget Tranfer Interface.

               
   
   REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   
   
*******************************************************************************/

procedure p_create_jv (p_jv_doc_id_in_out IN OUT fgbjvcd.fgbjvcd_doc_num%type,
P_acci_code_in in fgbjvcd.fgbjvcd_acci_code%type,
P_coas_code_in in	fgbjvcd.fgbjvcd_coas_code%type,
P_fund_code_in in	fgbjvcd.fgbjvcd_fund_code%type,
P_orgn_code_in in	fgbjvcd.fgbjvcd_orgn_code%type,
P_acct_code_in in	fgbjvcd.fgbjvcd_acct_code%type,
P_prog_code_in in	fgbjvcd.fgbjvcd_prog_code%type,
P_actv_code_in in	fgbjvcd.fgbjvcd_actv_code%type,
P_locn_code_in in	fgbjvcd.fgbjvcd_locn_code%type,
P_dr_cr_ind_in in	fgbjvcd.fgbjvcd_dr_cr_ind%type,
P_line_amt in	fgbjvcd.fgbjvcd_trans_amt%TYPE,
P_doc_total_amt	in fgbjvcd.fgbjvcd_trans_amt%TYPE,
P_change_type	in fgbjvcd.fgbjvcd_status_ind%type,
P_user_id_in in	fgbjvcd.fgbjvcd_user_id%type,
p_doc_desc in varchar2,
P_status_ind IN OUT	fgbjvcd.fgbjvcd_status_ind%type,
p_err_msg IN OUT VARCHAR2);

procedure p_create_jv_dtl ( 
p_jv_doc_id_in_out IN OUT fgbjvcd.fgbjvcd_doc_num%type,
P_acci_code_in	in fgbjvcd.fgbjvcd_acci_code%type,
P_coas_code_in	in fgbjvcd.fgbjvcd_coas_code%type,
P_fund_code_in	in fgbjvcd.fgbjvcd_fund_code%type,
P_orgn_code_in	in fgbjvcd.fgbjvcd_orgn_code%type,
P_acct_code_in	in fgbjvcd.fgbjvcd_acct_code%type,
P_prog_code_in	in fgbjvcd.fgbjvcd_prog_code%type,
P_actv_code_in	in fgbjvcd.fgbjvcd_actv_code%type,
P_locn_code_in	in fgbjvcd.fgbjvcd_locn_code%type,
P_dr_cr_ind_in	in fgbjvcd.fgbjvcd_dr_cr_ind%type,
P_line_amt	in fgbjvcd.fgbjvcd_trans_amt%TYPE,
P_doc_total_amt	in fgbjvcd.fgbjvcd_trans_amt%TYPE,
P_change_type	in fgbjvcd.fgbjvcd_status_ind%type,
P_user_id_in	in fgbjvcd.fgbjvcd_user_id%type,
p_doc_desc in varchar2,
P_status_ind IN OUT	fgbjvcd.fgbjvcd_status_ind%type,
p_err_msg IN OUT VARCHAR2);

procedure p_complete_jv (
		P_jv_doc_id	fgbjvcd.fgbjvcd_doc_num%type,
    P_user_id	fgbjvcd.fgbjvcd_user_id%type,
    P_status_ind	IN OUT fgbjvcd.fgbjvcd_status_ind%type,
    p_err_msg IN OUT VARCHAR2);
    
procedure f_get_avail_bal (
P_coas_code	Varchar2,
P_fund_code	Varchar2,
P_orgn_code	Varchar2,
P_acct_code	Varchar2,
P_prog_code	Varchar2,
p_avail_bal IN OUT NUMBER);

procedure f_get_jv_id (p_jv_id IN OUT fgbjvcd.fgbjvcd_doc_num%type);

PROCEDURE P_Check_Security(  in_coas IN FTVCOAS.FTVCOAS_COAS_CODE%TYPE,
                             in_fund IN FTVFUND.FTVFUND_FUND_CODE%TYPE,
                             in_orgn IN FTVORGN.FTVORGN_ORGN_CODE%TYPE,
                             in_user IN GOBEACC.GOBEACC_USER_ID%TYPE,
                             no_edit_errors IN OUT BOOLEAN, 
                             edit_error_msg IN OUT VARCHAR2
                           );
FUNCTION F_Security_For_Query(coas_code CHAR,
                                  fund_code CHAR,
                                   org_code CHAR,
                                  user_code CHAR)
    RETURN VARCHAR2;
    
procedure GET_SECURITY_INDEX_LIST(p_uni  in varchar2,
                                  p_Index in varchar2, 
                                  p_list  OUT FinIndxTab );  

function F_SECURITY_INDEX_LIST(p_uni  in varchar2,
                                p_Index in varchar2) return FININDXTAB PIPELINED;
  

procedure populate_fzrbxfr;

end bzebxfr;
/


CREATE OR REPLACE package body bzebxfr as
/*******************************************************************************

   NAME:       BZEBXFR

   PURPOSE:    This package is a part of the Budget Tranfer Interface.

               Procedure P_Check_Security is a copy of the Banner delivered bwfktran
               Function F_Security_For_Query is modified from bwfktran to select query access
               
               Procedure populate_fzbxfr populates the custom table fzrbxfr with indexes for all uni.  
                        The procedure is called from the microsoft application and the resulting table is queried
                        by the application.  Microsoft Sql cannot handle passing ref cursor or tables as parms.
                        
               Procedure get_security_index_list and f_security_index_list are an earlier attempt to return a table.
                        They are kept here for future use.  The PIPELINED function did improve the performance
                        of the returned records.
                        They utilitze two custom types which would need to be created in production.
                        create type FinIndxTabType as object(index_code varchar2(6));
                        create type FinIndxTab as table of FinIndxTabType;

               
   
   REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   05/15/2018  D. Semar      Added checksum to p_complete_jv 
   10/29/2018  D. Semar      Added fzrappr to populate_fzrbxfr
   
*******************************************************************************/
-------------------------------------------------------------------------------------

procedure p_create_jv ( 
p_jv_doc_id_in_out IN OUT fgbjvcd.fgbjvcd_doc_num%type,
P_acci_code_in	in fgbjvcd.fgbjvcd_acci_code%type,
P_coas_code_in	in fgbjvcd.fgbjvcd_coas_code%type,
P_fund_code_in	in fgbjvcd.fgbjvcd_fund_code%type,
P_orgn_code_in	in fgbjvcd.fgbjvcd_orgn_code%type,
P_acct_code_in	in fgbjvcd.fgbjvcd_acct_code%type,
P_prog_code_in	in fgbjvcd.fgbjvcd_prog_code%type,
P_actv_code_in	in fgbjvcd.fgbjvcd_actv_code%type,
P_locn_code_in	in fgbjvcd.fgbjvcd_locn_code%type,
P_dr_cr_ind_in	in fgbjvcd.fgbjvcd_dr_cr_ind%type,
P_line_amt	in fgbjvcd.fgbjvcd_trans_amt%TYPE,
P_doc_total_amt	in fgbjvcd.fgbjvcd_trans_amt%TYPE,
P_change_type	in fgbjvcd.fgbjvcd_status_ind%type,
P_user_id_in	in fgbjvcd.fgbjvcd_user_id%type,
p_doc_desc in varchar2,
P_status_ind IN OUT	fgbjvcd.fgbjvcd_status_ind%type,
p_err_msg IN OUT VARCHAR2) is

banner_user_id varchar2(30);
l_row_id gb_common.internal_record_id_type;
c_doc_descr varchar2(35);                     
c_data_origin varchar2(30) := 'XFERAPP';
c_ref_num  varchar2(8) := '000000';           -- need to find out if he has an internal ref num
c_rucl_code varchar2(4);
l_warning_out varchar2(4000);
l_abal_severity_out varchar2(1);
l_fsyr varchar2(2);
l_fspd varchar2(2);
v_complete varchar2(4000);
l_jv_exists varchar2(1);
l_jv_posted varchar2(1);
l_cr_jv varchar2(1);
l_status_in_out varchar2(1);
nxt_seq number := 0;

no_jv_id_error EXCEPTION;
pragma exception_init(no_jv_id_error, -20000);

--l_edit_errors varchar2(1) := 'N';
begin

  p_status_ind := 'N';
  l_jv_exists := 'N';
  l_jv_posted := 'N';
  l_cr_jv := 'N';
  p_err_msg := 'Initializing variables ';
  c_doc_descr := p_user_id_in ||'-'||to_char(sysdate,'yyyymmdd');
  
  /* check for null jv id */
  
  if p_jv_doc_id_in_out is null or p_jv_doc_id_in_out = '0' then
     raise no_jv_id_error;
  end if;
  
 begin
 /*get the banner id for the user */
   p_err_msg := 'Getting Banner Id ';
  select gobeacc_username
    into banner_user_id
    from gobeacc
    where gobeacc_pidm = (select distinct spriden_pidm  from spriden where spriden_id = 'JDA73');
  exception
    when no_data_found then p_status_ind := 'E'; p_err_msg := 'Banner ID not found';
    when others then null;
  end;
  
    p_err_msg := 'Determining RUCL Code';
  case p_change_type when 'T' then c_rucl_code := 'BD04';
                     when 'P' then c_rucl_code := 'BD02';
  end case;
    p_err_msg := 'Selecting fiscal year/period ';
   SELECT FTVFSYR_FSYR_CODE,FTVFSPD_FSPD_CODE
      INTO l_fsyr, l_fspd
      FROM FTVFSPD,FTVFSYR
     WHERE (FTVFSPD_PRD_START_DATE <= trunc(SYSDATE)
            AND FTVFSPD_PRD_END_DATE >= trunc(SYSDATE))
       AND FTVFSPD_FSYR_CODE=FTVFSYR_FSYR_CODE
       AND FTVFSPD_COAS_CODE=FTVFSYR_COAS_CODE
       AND FTVFSPD_COAS_CODE='1'
       AND (FTVFSYR_START_DATE <= trunc(SYSDATE)
            AND FTVFSYR_END_DATE >= trunc(SYSDATE));

 /* Check to see if the JV already exists.  If not, then create the header record.  If it does and
     a)  it is in the JV table then it is in progress and we can skip the header.
     b)  it has posted, then return with an error status */

   /* is it an inprogress JV ?*/
  begin
    p_err_msg := 'In JVCH?';
   select 'I' into l_jv_exists
     from fgbjvch
    where fgbjvch_doc_num = p_jv_doc_id_in_out;
    
     p_status_ind := 'I';
     p_err_msg := 'JV Header already exists';
     
  exception
    when no_data_found then l_jv_exists := 'N';
    when others then --dbms_output.put_line('Testing doc exist in jvch');
                    --dbms_output.put_line(sqlerrm);
                    p_status_ind := 'E';
                    p_err_msg := sqlerrm;
  end;
  
   /* is it a posted JV ? */
  begin
 --   p_err_msg := 'p_create_jv: In TRND?'||to_char(p_doc_total_amt);
   select 'P' into l_jv_posted
     from fgbtrnh
     where fgbtrnh_doc_code = p_jv_doc_id_in_out;    
     p_status_ind := 'E';
     p_err_msg := 'JV is already posted';
 
 exception
    when no_data_found then l_jv_posted := 'N';
    when others then -- dbms_output.put_line('Testing doc exist in trnh');
                    -- dbms_output.put_line(sqlerrm);
                    p_status_ind := 'E';
                    p_err_msg := sqlerrm;
  end;
  
  if l_jv_exists = 'N' and l_jv_posted = 'N' then
    l_cr_jv := 'Y';
  else
    l_cr_jv := 'N';
  end if;
  
  if l_cr_jv = 'Y' and p_status_ind <> 'E' then
  begin
  /* Create the header */
  p_err_msg := 'Creating JV Header';
  fb_jv_header.p_create(p_doc_num_in_out => p_jv_doc_id_in_out, 
                        p_submission_number => 0, 
                        p_user_id => banner_user_id, 
                        p_trans_date => trunc(sysdate),
                        p_doc_description => p_doc_desc,   --c_doc_descr,
                        p_doc_amt => p_doc_total_amt*2,    -- this is a control total and must equal the sum of all transactions 
                        p_auto_jrnl_id => NULL,
                        p_reversal_ind => NULL,
                        p_obud_code => NULL,
                        p_obph_code => NULL,
                        p_budg_dur_code => NULL,
                        p_edit_defer_ind => 'N',
                        p_status_ind => 'I',
                        p_approval_ind => 'N',
                        p_distrib_amt => null,
                        p_nsf_on_off_ind => null,
                        p_data_origin => c_data_origin,
                        p_rowid_out => l_row_id);
        p_err_msg := 'JV Header created. ';
        p_status_ind := 'C';
 exception
   when others then
       -- dbms_output.put_line('Error creating JV header: '||p_jv_doc_id_in_out);
       -- dbms_output.put_line(sqlerrm);
        p_status_ind := 'E';
        p_err_msg := sqlerrm;
  end;
 end if;
 exception
   when no_jv_id_error then
      p_status_ind := 'E';
      p_err_msg := 'JV Doc Num is null';
   when others then
      --  dbms_output.put_line('Error creating JV header: '||p_jv_doc_id_in_out);
      --  dbms_output.put_line(sqlerrm);
        p_status_ind := 'E';
        p_err_msg := sqlerrm;
end;

-------------------------------------------------------------------------------------

procedure p_create_jv_dtl ( 
p_jv_doc_id_in_out IN OUT fgbjvcd.fgbjvcd_doc_num%type,
P_acci_code_in in	fgbjvcd.fgbjvcd_acci_code%type,
P_coas_code_in	in fgbjvcd.fgbjvcd_coas_code%type,
P_fund_code_in in fgbjvcd.fgbjvcd_fund_code%type,
P_orgn_code_in in	fgbjvcd.fgbjvcd_orgn_code%type,
P_acct_code_in in	fgbjvcd.fgbjvcd_acct_code%type,
P_prog_code_in in	fgbjvcd.fgbjvcd_prog_code%type,
P_actv_code_in in	fgbjvcd.fgbjvcd_actv_code%type,
P_locn_code_in in	fgbjvcd.fgbjvcd_locn_code%type,
P_dr_cr_ind_in in	fgbjvcd.fgbjvcd_dr_cr_ind%type,
P_line_amt in	fgbjvcd.fgbjvcd_trans_amt%TYPE,
P_doc_total_amt in	fgbjvcd.fgbjvcd_trans_amt%TYPE,
P_change_type	in fgbjvcd.fgbjvcd_status_ind%type,
P_user_id_in in	fgbjvcd.fgbjvcd_user_id%type,
p_doc_desc in varchar2,
P_status_ind IN OUT	fgbjvcd.fgbjvcd_status_ind%type,
p_err_msg IN OUT VARCHAR2) is

banner_user_id varchar2(30);
l_row_id gb_common.internal_record_id_type;
c_doc_descr varchar2(35);                     
c_data_origin varchar2(30) := 'XFERAPP';
c_ref_num  varchar2(8) := '000000';           -- need to find out if he has an internal ref num
c_rucl_code varchar2(4);
l_warning_out varchar2(4000);
l_abal_severity_out varchar2(1);
l_fsyr varchar2(2);
l_fspd varchar2(2);
v_complete varchar2(4000);
l_jv_exists varchar2(1);
l_jv_posted varchar2(1);
l_cr_detail varchar2(1);
l_status_in_out varchar2(1);
nxt_seq number := 0;

no_jv_id_error EXCEPTION;
pragma exception_init(no_jv_id_error,-20000);

--l_edit_errors varchar2(1) := 'N';
begin

  p_status_ind := 'N';
  l_jv_exists := 'N';
  l_jv_posted := 'N';
  l_cr_detail := 'N';
  p_err_msg := 'Initializing variables ';
  c_doc_descr := p_user_id_in ||'-'||to_char(sysdate,'yyyymmdd');

  if p_jv_doc_id_in_out is null or p_jv_doc_id_in_out = '0' then
    raise no_jv_id_error;
  end if;
  
 begin
 /*get the banner id for the user */
   p_err_msg := 'Getting Banner Id ';
  select gobeacc_username
    into banner_user_id
    from gobeacc
    where gobeacc_pidm = (select distinct spriden_pidm  from spriden where spriden_id = 'JDA73');
  exception
    when no_data_found then p_status_ind := 'E'; p_err_msg := 'Banner ID not found';
    when others then null;
  end;
  
    p_err_msg := 'Determining RUCL Code';
  case p_change_type when 'T' then c_rucl_code := 'BD04';
                     when 'P' then c_rucl_code := 'BD02';
  end case;
    p_err_msg := 'Selecting fiscal year/period ';
   SELECT FTVFSYR_FSYR_CODE,FTVFSPD_FSPD_CODE
      INTO l_fsyr, l_fspd
      FROM FTVFSPD,FTVFSYR
     WHERE (FTVFSPD_PRD_START_DATE <= trunc(SYSDATE)
            AND FTVFSPD_PRD_END_DATE >= trunc(SYSDATE))
       AND FTVFSPD_FSYR_CODE=FTVFSYR_FSYR_CODE
       AND FTVFSPD_COAS_CODE=FTVFSYR_COAS_CODE
       AND FTVFSPD_COAS_CODE='1'
       AND (FTVFSYR_START_DATE <= trunc(SYSDATE)
            AND FTVFSYR_END_DATE >= trunc(SYSDATE));

 /* Check to see if the JV already exists.  If not, then create the header record.  If it does and
     a)  it is in the JV table then it is in progress and we can skip the header.
     b)  it has posted, then return with an error status */

   /* is it an inprogress JV ?*/
  begin
    p_err_msg := 'In JVCH?';
  
   select 'Y' into l_jv_exists
     from fgbjvch
    where fgbjvch_doc_num = p_jv_doc_id_in_out;
    
    p_status_ind := 'C';
    p_err_msg := 'JV in progress';
    
  exception
    when no_data_found then l_jv_exists := 'N'; 
                            p_status_ind := 'E'; 
                            p_err_msg := 'No header record found';
    when others then --dbms_output.put_line('Testing doc exist in jvch');
                    --dbms_output.put_line(sqlerrm);
                    p_status_ind := 'E';
                    p_err_msg := substr(sqlerrm,1,100);
  end;
  
   /* is it a posted JV ? */
  begin
    p_err_msg := 'p_create_jv_dtl: In TRND?';
   select 'P' into l_jv_posted
     from fgbtrnh
     where fgbtrnh_doc_code = p_jv_doc_id_in_out;    
     p_status_ind := 'E';
     p_err_msg := 'JV is already posted';
 
 exception
    when no_data_found then l_jv_posted := 'N';
    when others then --dbms_output.put_line('Testing doc exist in trnh');
                    --dbms_output.put_line(sqlerrm);
                    p_status_ind := 'E';
                    p_err_msg := substr(sqlerrm,1,100);
  end;
  
/* Now for the detail lines */
/* This will reduce to a single call based on the +/- indicator that is sent in */

 if l_jv_exists = 'Y' and l_jv_posted = 'N' then
    l_cr_detail := 'Y';
 else
    l_cr_detail := 'N';
 end if;
 
if l_cr_detail = 'Y' and p_status_ind <> 'E' then

begin
  p_err_msg := 'Selecting next seq for detail line';
  begin
    select nvl(max(fgbjvcd_seq_num),0)+ 1 into nxt_seq
     from fgbjvcd
     where fgbjvcd_doc_num = p_jv_doc_id_in_out
     and fgbjvcd_submission_number = 0;
  exception
     when no_data_found then nxt_seq := 1;
     when others then null;
  end;

   p_err_msg := 'Creating detail line ';
     
   fb_jv_detail.p_create(p_doc_num =>trim(p_jv_doc_id_in_out),
                         p_submission_number => 0,
                         p_seq_num => nxt_seq,
                         p_user_id => banner_user_id,
                         p_rucl_code => c_rucl_code, 
                         p_trans_amt => p_line_amt,
                         p_trans_desc => p_doc_desc,     --c_doc_descr,
                         p_dr_cr_ind => trim(p_dr_cr_ind_in),
                         p_acci_code => trim(p_acci_code_in),
                         p_coas_code => trim(p_coas_code_in),
                         p_fund_code => trim(p_fund_code_in),
                         p_orgn_code => trim(p_orgn_code_in),
                         p_acct_code => trim(p_acct_code_in),
                         p_prog_code => trim(p_prog_code_in),
                         p_actv_code => trim(p_actv_code_in),
                         p_locn_code => trim(p_locn_code_in),
                         p_abal_override => 'N',
                         p_fsyr_code => l_fsyr,
                         p_posting_period => l_fspd,
                         p_budget_period => l_fspd,   -- This is being set to the same as the current fiscal period, but on SSB it is selected.
                         p_doc_ref_num => c_ref_num,                        
                         p_appr_ind => 'N',
                         p_data_origin => c_data_origin,
                         p_status_in_out => l_status_in_out,
                         p_warning_out => l_warning_out,
                         p_abal_severity_out => l_abal_severity_out,
                         p_rowid_out => l_row_id);
                         
  if l_status_in_out is not null and l_status_in_out <> 'P' then
  p_status_ind := l_status_in_out;  --'E';
  p_err_msg := nvl(l_abal_severity_out,substr(l_warning_out,1,100));
  else
  p_status_ind := 'C';
  p_err_msg := 'Created detail seqno '||to_char(nxt_seq);
  end if;
  
 exception
   when others then
    --    dbms_output.put_line('Error detail record for JV: '||p_jv_doc_id_in_out);
    --    dbms_output.put_line(sqlerrm);
    --    dbms_output.put_line('Status: '||l_status_in_out);
    --    dbms_output.put_line('Warning: '||l_warning_out);
    --    dbms_output.put_line('Severity: '||l_abal_severity_out);
        p_status_ind := 'E';
        p_err_msg := substr(sqlerrm,1,100);
       -- p_err_msg := p_dr_cr_ind_in||','||p_acci_code_in||','||p_coas_code_in||','||p_fund_code_in||','||p_orgn_code_in||','||p_acct_code_in||','||p_prog_code_in||','||p_actv_code_in||','||p_locn_code_in;
end;
end if;
exception
  when no_jv_id_error then
     p_status_ind := 'E';
     p_err_msg := 'JV doc num is null';
  when others then 
  --dbms_output.put_line('Overall error: ');
  --dbms_output.put_line(sqlerrm);
  p_status_ind := 'E';
  p_err_msg := substr(sqlerrm,1,100);
end;
-------------------------------------------------------------------------------------

procedure p_complete_jv (
		P_jv_doc_id	fgbjvcd.fgbjvcd_doc_num%type,
    P_user_id	fgbjvcd.fgbjvcd_user_id%type,
    P_status_ind	IN OUT fgbjvcd.fgbjvcd_status_ind%type,
    p_err_msg IN OUT VARCHAR2) is

banner_user_id varchar2(30);
v_complete varchar2(4000);
chksum number:=0;
begin
  begin
 /*get the banner id for the user */
  select gobeacc_username
    into banner_user_id
    from gobeacc
    where gobeacc_pidm = (select distinct spriden_pidm  from spriden where spriden_id = 'JDA73');
  exception
    when no_data_found then p_status_ind := 'E';
    when others then null;
  end;

 /* Perform checksum on JV */
  begin
  select sum( decode(fgbjvcd_dr_cr_ind,'+',fgbjvcd_trans_amt,0)) - sum(decode(fgbjvcd_dr_cr_ind,'-',fgbjvcd_trans_amt,0)) 
    into chksum
    from FIMSMGR.FGBJVCD
    where FGBJVCD_DOC_NUM = p_jv_doc_id;

    if chksum <> 0 then
     p_status_ind := 'E';
     p_err_msg := 'JV is out of balance';
    end if;
    
  exception
     when others then p_status_ind := 'E'; p_err_msg := sqlerrm;
  end;


 if p_status_ind <> 'E' then
   begin
     fp_journal_voucher.p_complete(
         p_doc_num            => p_jv_doc_id,
         p_submission_number  => 0,
         p_complete_requested => 'Y',
         p_user_id            => banner_user_id,
         p_msg_out            => v_complete); 
     
           p_status_ind := 'C';   
           p_err_msg := nvl(v_complete,'Done');
         
     exception
       when others then dbms_output.put_line(sqlerrm); 
                        dbms_output.put_line(v_complete);
                        p_status_ind := 'E';
     end;
 end if;
 
end;
-------------------------------------------------------------------------------------

procedure f_get_avail_bal (
P_coas_code	Varchar2,
P_fund_code	Varchar2,
P_orgn_code	Varchar2,
P_acct_code	Varchar2,
P_prog_code	Varchar2,
p_avail_bal IN OUT number) is
a_val number := 9999;
begin 
    
SELECT   NVL(decode(FTVFSPD_FSPD_CODE,
                    '00', FGBOPAL_00_adopt_bud + fgbopal_00_bud_adjt - fgbopal_00_ytd_actv - fgbopal_00_encumb - fgbopal_00_bud_rsrv,
                    '01', fgbopal_01_adopt_bud + fgbopal_01_bud_adjt - fgbopal_01_ytd_actv - fgbopal_01_encumb - fgbopal_01_bud_rsrv,
                    '02', fgbopal_02_adopt_bud + fgbopal_02_bud_adjt - fgbopal_02_ytd_actv - fgbopal_02_encumb - fgbopal_02_bud_rsrv,
                    '03', fgbopal_03_adopt_bud + fgbopal_03_bud_adjt - fgbopal_03_ytd_actv - fgbopal_03_encumb - fgbopal_03_bud_rsrv,
                    '04', fgbopal_04_adopt_bud + fgbopal_04_bud_adjt - fgbopal_04_ytd_actv - fgbopal_04_encumb - fgbopal_04_bud_rsrv,
                    '05', fgbopal_05_adopt_bud + fgbopal_05_bud_adjt - fgbopal_05_ytd_actv - fgbopal_05_encumb - fgbopal_05_bud_rsrv,
                    '06', fgbopal_06_adopt_bud + fgbopal_06_bud_adjt - fgbopal_06_ytd_actv - fgbopal_06_encumb - fgbopal_06_bud_rsrv,
                    '07', fgbopal_07_adopt_bud + fgbopal_07_bud_adjt - fgbopal_07_ytd_actv - fgbopal_07_encumb - fgbopal_07_bud_rsrv,
                    '08', fgbopal_08_adopt_bud + fgbopal_08_bud_adjt - fgbopal_08_ytd_actv - fgbopal_08_encumb - fgbopal_08_bud_rsrv,
                    '09', fgbopal_09_adopt_bud + fgbopal_09_bud_adjt - fgbopal_09_ytd_actv - fgbopal_09_encumb - fgbopal_09_bud_rsrv,
                    '10', fgbopal_10_adopt_bud + fgbopal_10_bud_adjt - fgbopal_10_ytd_actv - fgbopal_10_encumb - fgbopal_10_bud_rsrv,
                    '11', fgbopal_11_adopt_bud + fgbopal_11_bud_adjt - fgbopal_11_ytd_actv - fgbopal_11_encumb - fgbopal_11_bud_rsrv,
                    '12', fgbopal_12_adopt_bud + fgbopal_12_bud_adjt - fgbopal_12_ytd_actv - fgbopal_12_encumb - fgbopal_12_bud_rsrv,
                    '13', fgbopal_13_adopt_bud + fgbopal_13_bud_adjt - fgbopal_13_ytd_actv - fgbopal_13_encumb - fgbopal_13_bud_rsrv,
                    '14', fgbopal_14_adopt_bud + fgbopal_14_bud_adjt - fgbopal_14_ytd_actv - fgbopal_14_encumb - fgbopal_14_bud_rsrv, 0), 0) 
    into p_avail_bal                
    FROM FGBOPAL,
         FTVFSPD,
         ftvacct, 
         ftvacci
    WHERE
          FTVFSPD_COAS_CODE = '1'
    AND   FGBOPAL_FSYR_CODE = FTVFSPD_FSYR_CODE
    AND   FGBOPAL_COAS_CODE = '1'
    and   trunc(sysdate) between trunc(ftvfspd_prd_start_date) and trunc(ftvfspd_prd_end_date) 
    and  fgbopal_acct_code = ftvacct_acct_code
    and  trunc(sysdate) between trunc(ftvacct_eff_date) and trunc(ftvacct_nchg_date)
    and  fgbopal_fund_code = ftvacci_fund_code
    and  fgbopal_orgn_code = ftvacci_orgn_code
    and  fgbopal_prog_code = ftvacci_prog_code
    and  trunc(sysdate) between trunc(ftvacci_eff_date) and trunc(ftvacci_nchg_date)    
    and fgbopal_coas_code = p_coas_code
    and fgbopal_fund_code = p_fund_code
    and fgbopal_orgn_code = p_orgn_code
    and fgbopal_acct_code = p_acct_code
    and fgbopal_prog_code = p_prog_code;

exception when others then p_avail_bal := 0; 
end;
-------------------------------------------------------------------------------------

procedure f_get_jv_id (p_jv_id IN OUT fgbjvcd.fgbjvcd_doc_num%type) is

jv_doc_id varchar2(8);

begin
    fokutil.p_gen_doc_code(doc_type => FB_JV_HEADER.M_JV_DOC_TYPE,
                           doc_code => jv_doc_id,
                           doc_length => 8,
                           doc_prefix => FB_JV_HEADER.M_JV_SEQ_TYPE);
    p_jv_id := nvl(jv_doc_id,0);   
exception
    when others then dbms_output.put_line(sqlerrm);
                     p_jv_id := '0';
end;
-------------------------------------------------------------------------------------
PROCEDURE P_Check_Security(  in_coas IN FTVCOAS.FTVCOAS_COAS_CODE%TYPE,
                              in_fund IN FTVFUND.FTVFUND_FUND_CODE%TYPE,
                              in_orgn IN FTVORGN.FTVORGN_ORGN_CODE%TYPE,
                              in_user IN GOBEACC.GOBEACC_USER_ID%TYPE,
                              no_edit_errors IN OUT BOOLEAN, 
                              edit_error_msg IN OUT VARCHAR2
                           ) IS
  secind    VARCHAR2(10);
  BEGIN
    IF ( in_coas IS NOT NULL ) AND (in_fund IS NOT NULL) THEN
      secind := F_Security_For_Query( in_coas, in_fund, in_orgn, in_user );
      IF secind= 'Fund' THEN
          --twbkfrmt.p_printmessage(G$_NLS.Get('BWFKTRA1-0002','SQL',
           --                      'No permission to use fund %01% ',in_fund),
           --                      'ERROR'
           --                      );
        edit_error_msg := 'No permission to use fund '||in_fund;
        no_edit_errors:=FALSE;
      END IF;

      IF secind= 'Org' THEN
        --twbkfrmt.p_printmessage(G$_NLS.Get('BWFKTRA1-0003','SQL',
        --                        'No permission to use orgn %01% ',in_orgn),
        --                        'ERROR'
        --                       );
        edit_error_msg := 'No permission to use orgn '||in_orgn;
        no_edit_errors:=FALSE;
      END IF;

      IF secind= 'FundOrg' THEN
/*        twbkfrmt.p_printmessage(G$_NLS.Get('BWFKTRA1-0004',
                                 'SQL',
                                 'No permission to use fund %01%.',
                                 in_fund),
                                 'ERROR'
                                 );
          twbkfrmt.p_printmessage(G$_NLS.Get('BWFKTRA1-0005','SQL',
                                 'No permission to use orgn %01% ',in_orgn),
                                 'ERROR'
                                 );
  */
        edit_error_msg := 'No permission to use fund/org: '||in_fund||'/'||in_orgn;
        no_edit_errors:=FALSE;
      END IF;
    END IF;
  END P_Check_Security;
 
  FUNCTION F_Security_For_Query(coas_code CHAR,
                                  fund_code CHAR,
                                   org_code CHAR,
                                  user_code CHAR)
    RETURN VARCHAR2 IS

    fund_code2 FTVFUND.FTVFUND_FUND_CODE%TYPE;
    ftyp_code  FTVFTYP.FTVFTYP_FTYP_CODE%TYPE;
    orgn_code  FTVORGN.FTVORGN_ORGN_CODE%TYPE;
       retval  VARCHAR2(10);
          val  VARCHAR2(1);


    CURSOR CheckMasterFundC IS
      SELECT FOBPROF_MASTER_FUND_IND
        FROM FOBPROF
       WHERE FOBPROF_USER_ID = user_code;


    CURSOR CheckFundHierC IS
      SELECT FTVFUND_FUND_CODE
        FROM FTVFUND
       WHERE FTVFUND_FUND_CODE IN  (
         SELECT FORUSFN_FUND_CODE
           FROM FORUSFN
          WHERE FORUSFN_USER_ID_ENTERED = user_code
            AND FORUSFN_COAS_CODE = coas_code
            AND FORUSFN_FUND_CODE IS NOT NULL )
         CONNECT BY FTVFUND_FUND_CODE = PRIOR FTVFUND_FUND_CODE_PRED
                AND  FTVFUND_COAS_CODE = PRIOR FTVFUND_COAS_CODE
                AND  FTVFUND_EFF_DATE <= SYSDATE
                AND  FTVFUND_NCHG_DATE > SYSDATE
              START WITH FTVFUND_FUND_CODE = fund_code
                AND  FTVFUND_COAS_CODE = coas_code
                AND  FTVFUND_EFF_DATE <= SYSDATE
                AND  FTVFUND_NCHG_DATE > SYSDATE ;


    CURSOR CheckFundC IS
      SELECT FORUSFN_ACCESS_IND
        FROM FORUSFN
       WHERE FORUSFN_FUND_CODE = fund_code2
         AND FORUSFN_COAS_CODE = coas_code
         AND FORUSFN_USER_ID_ENTERED = user_code ;


    CURSOR CheckFtypHierC IS
      SELECT FTVFTYP_FTYP_CODE
        FROM FTVFTYP
       WHERE FTVFTYP_FTYP_CODE IN  (
         SELECT FORUSFN_FTYP_CODE
           FROM FORUSFN
          WHERE FORUSFN_USER_ID_ENTERED = user_code
            AND FORUSFN_COAS_CODE = coas_code
            AND FORUSFN_FTYP_CODE IS NOT NULL )
         CONNECT BY FTVFTYP_FTYP_CODE = PRIOR FTVFTYP_FTYP_CODE_PRED
                AND  FTVFTYP_COAS_CODE = PRIOR FTVFTYP_COAS_CODE
                AND  FTVFTYP_EFF_DATE <= SYSDATE
                AND  FTVFTYP_NCHG_DATE > SYSDATE
              START WITH FTVFTYP_FTYP_CODE IN  (
                    SELECT FTVFUND_FTYP_CODE
                      FROM FTVFUND
                     WHERE FTVFUND_COAS_CODE = coas_code
                       AND FTVFUND_FUND_CODE = fund_code
                       AND FTVFUND_EFF_DATE <= SYSDATE
                       AND FTVFUND_NCHG_DATE > SYSDATE )
                AND  FTVFTYP_COAS_CODE = coas_code
                AND  FTVFTYP_EFF_DATE <= SYSDATE
                AND  FTVFTYP_NCHG_DATE > SYSDATE ;


    CURSOR CheckFtypC IS
      SELECT FORUSFN_ACCESS_IND
        FROM FORUSFN
       WHERE FORUSFN_FTYP_CODE = ftyp_code
         AND FORUSFN_COAS_CODE = coas_code
         AND FORUSFN_USER_ID_ENTERED = user_code ;

    CURSOR CheckOrgnHierC IS
      SELECT FTVORGN_ORGN_CODE
        FROM FTVORGN
       WHERE FTVORGN_ORGN_CODE IN  (
         SELECT FORUSOR_ORGN_CODE
           FROM FORUSOR
          WHERE FORUSOR_USER_ID_ENTERED = user_code
            AND FORUSOR_COAS_CODE = coas_code )
         CONNECT BY FTVORGN_ORGN_CODE = PRIOR FTVORGN_ORGN_CODE_PRED
                      AND  FTVORGN_COAS_CODE = PRIOR FTVORGN_COAS_CODE
                      AND  FTVORGN_EFF_DATE <= SYSDATE
                      AND  FTVORGN_NCHG_DATE > SYSDATE
                    START WITH FTVORGN_ORGN_CODE = org_code
                      AND  FTVORGN_COAS_CODE = coas_code
                      AND  FTVORGN_EFF_DATE <= SYSDATE
                      AND  FTVORGN_NCHG_DATE > SYSDATE ;


    CURSOR CheckMasterOrgnC IS
      SELECT FOBPROF_MASTER_ORGN_IND
        FROM FOBPROF
       WHERE FOBPROF_USER_ID = user_code;


    CURSOR CheckOrgnC IS
      SELECT FORUSOR_ACCESS_IND
        FROM FORUSOR
       WHERE FORUSOR_ORGN_CODE = orgn_code
         AND FORUSOR_COAS_CODE = coas_code
         AND FORUSOR_USER_ID_ENTERED = user_code ;

  BEGIN
    retval := NULL;
      OPEN  CheckMasterFundC;
      FETCH CheckMasterFundC INTO val;
      IF CheckMasterFundC%FOUND THEN
        IF val IN ('Q','B') THEN
          CLOSE CheckMasterFundC;
          GOTO check_org;
        END IF;
      END IF;
      CLOSE CheckMasterFundC;

    OPEN  CheckFundHierC;
    FETCH CheckFundHierC INTO fund_code2;
    IF CheckFundHierC%FOUND THEN
      OPEN CheckFundC;
      FETCH CheckFundC INTO val;
      IF CheckFundC%found THEN
        IF val IN ('Q','B') THEN
          CLOSE CheckFundHierC;
          CLOSE CheckFundC;
          GOTO check_org;
        ELSE
          CLOSE CheckFundHierC;
          CLOSE CheckFundC;
          retval:= 'Fund' ;
          GOTO check_org;
        END IF;
      ELSE
        CLOSE CheckFundHierC;
        CLOSE CheckFundC;
        retval:= 'Fund' ;
        GOTO check_org;
      END IF;
    END IF;
    CLOSE CheckFundHierC;

    OPEN  CheckFtypHierC;
    FETCH CheckFtypHierC INTO ftyp_code;
    IF CheckFtypHierC%notfound THEN
       CLOSE CheckFtypHierC;
       retval:= 'Fund' ;
       goto check_org;
    END IF;
    CLOSE CheckFtypHierC;

    OPEN  CheckFtypC;
    FETCH CheckFtypC into val;
    IF CheckFtypC%NOTFOUND THEN
       CLOSE CheckFtypC;
       retval:= 'Fund' ;
       GOTO check_org;
    END IF;
    CLOSE CheckFtypC;

    IF val NOT IN ('Q','B') THEN
       retval:= 'Fund' ;
       goto check_org;
    END IF;

<<check_org>>
    IF org_code IS NULL THEN
      IF retval IS NULL THEN
         RETURN 'Y';
      ELSE
         RETURN retval;
      END IF;
    END IF;

    OPEN  CheckMasterOrgnC;
    FETCH CheckMasterOrgnC INTO val;
    CLOSE CheckMasterOrgnC;
    IF val IN ('Q','B') THEN
      IF retval IS NULL THEN
        RETURN 'Y';
      ELSE
        RETURN retval;
      END IF;
    END IF;

    OPEN  CheckOrgnHierC;
    FETCH CheckOrgnHierC INTO orgn_code;
    IF CheckOrgnHierC%NOTFOUND THEN
       CLOSE CheckOrgnHierC;
       RETURN  retval||'Org';
    END IF;
    CLOSE CheckOrgnHierC;

    OPEN CheckOrgnC;
    FETCH CheckOrgnC INTO val;
    IF CheckOrgnC%NOTFOUND THEN
       CLOSE CheckOrgnC;
       RETURN  retval||'Org' ;
    END IF;
    CLOSE CheckOrgnC;

    IF val NOT IN ('Q','B') THEN
      RETURN retval||'Org' ;
    END IF;
    IF retval IS NULL THEN
      RETURN 'Y';
    ELSE
      RETURN retval;
    END IF;
  END F_Security_For_Query;  
/*===========================*/
procedure GET_SECURITY_INDEX_LIST(p_uni  in varchar2,
                                  p_Index in varchar2, 
                                  p_list  OUT FinIndxTab ) IS 

tab_indx        number := 1;
l_user_code     gobeacc.gobeacc_username%TYPE;
has_master_lvl  varchar2(1) := 'N';
acci_code       varchar2(6);
stmt_id         varchar2(100);

CURSOR GetAll is
     select ftvacci_acci_code 
      from ftvacci
      where ftvacci_coas_code = '1'
      and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
      and ftvacci_status_ind = 'A'
      and nvl(trunc(ftvacci_term_date), trunc(sysdate+1)) > trunc(SYSDATE)
      and ftvacci_acci_code like p_Index;

CURSOR GetFundHierC(fund_code_in varchar2, l_user_code varchar2) IS
      SELECT FTVFUND_FUND_CODE
        FROM FTVFUND
       WHERE FTVFUND_FUND_CODE IN  (
         SELECT FORUSFN_FUND_CODE
           FROM FORUSFN
          WHERE FORUSFN_USER_ID_ENTERED = l_user_code
            AND FORUSFN_COAS_CODE = '1'
            AND FORUSFN_FUND_CODE IS NOT NULL )
         CONNECT BY FTVFUND_FUND_CODE = PRIOR FTVFUND_FUND_CODE_PRED
                AND  FTVFUND_COAS_CODE = PRIOR FTVFUND_COAS_CODE
                AND  FTVFUND_EFF_DATE <= SYSDATE
                AND  FTVFUND_NCHG_DATE > SYSDATE
              START WITH FTVFUND_FUND_CODE = fund_code_in
                AND  FTVFUND_COAS_CODE = '1'
                AND  FTVFUND_EFF_DATE <= SYSDATE
                AND  FTVFUND_NCHG_DATE > SYSDATE ;
                
   CURSOR GetFundC IS
      SELECT FORUSFN_FUND_CODE
        FROM FORUSFN
      WHERE FORUSFN_COAS_CODE = '1'
         AND FORUSFN_USER_ID_ENTERED = l_user_code ;
         
    CURSOR GetOrgnHierC(orgn_code_in varchar2, l_user_code varchar2) IS
      SELECT FTVORGN_ORGN_CODE
        FROM FTVORGN
       WHERE FTVORGN_ORGN_CODE IN  (
         SELECT FORUSOR_ORGN_CODE
           FROM FORUSOR
          WHERE FORUSOR_USER_ID_ENTERED = l_user_code
            AND FORUSOR_COAS_CODE = '1')
         CONNECT BY FTVORGN_ORGN_CODE = PRIOR FTVORGN_ORGN_CODE_PRED
                      AND  FTVORGN_COAS_CODE = PRIOR FTVORGN_COAS_CODE
                      AND  FTVORGN_EFF_DATE <= SYSDATE
                      AND  FTVORGN_NCHG_DATE > SYSDATE
                    START WITH FTVORGN_ORGN_CODE = orgn_code_in
                      AND  FTVORGN_COAS_CODE = '1'
                      AND  FTVORGN_EFF_DATE <= SYSDATE
                      AND  FTVORGN_NCHG_DATE > SYSDATE ;
 
 CURSOR GetOrgnC IS
      SELECT FORUSOR_ORGN_CODE
        FROM FORUSOR
       WHERE FORUSOR_COAS_CODE = '1'
         AND FORUSOR_USER_ID_ENTERED = l_user_code ;
begin

   p_list := FININDXTAB();
   
  /* get the username for the uni passed in */
  stmt_id := 'Get pidm';
  select gobeacc_username
    into l_user_code
    from spriden, gobeacc
    where spriden_pidm = gobeacc_pidm 
    and spriden_ntyp_code = 'UNI'
    and spriden_id = p_uni;

  /* check for master level access */
  stmt_id := 'Check master level access';
  begin 
     select 'Y'
      into has_master_lvl
      FROM FOBPROF
       WHERE FOBPROF_USER_ID = l_user_code
       AND (fobprof_master_fund_ind is not null or fobprof_master_orgn_ind is not null);
       
  exception
     when no_data_found then has_master_lvl := 'N';
     when others then dbms_output.put_line(sqlerrm);
  end;
  
  /* if the user has master level acces, return all indexes */
  stmt_id := 'Selecting indexes.  Master lvl is '||has_master_lvl;
  if has_master_lvl = 'Y' then
     for i in GetAll loop
       p_list.extend;
       p_list(tab_indx) := FinIndxTabType(i.ftvacci_acci_code);
       tab_indx := tab_indx + 1;
      end loop;
  else
      for i in GetFundC loop
          for j in GetFundHierC(i.forusfn_fund_code, l_user_code) loop
               select ftvacci_acci_code
                 into acci_code
                 from ftvacci
                 where ftvacci_fund_code = j.ftvfund_fund_code
                 and ftvacci_coas_code = '1'
                 and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
                 and ftvacci_status_ind = 'A'
                 and trunc(ftvacci_term_date) > trunc(SYSDATE)
                 and ftvacci_acci_code like p_Index;
          p_list.extend;
          p_list(tab_indx) :=  FinIndxTabType(acci_code);
          tab_indx := tab_indx + 1;
        end loop;
     end loop;
 stmt_id := 'Selecting Orgs';
    for i in GetOrgnC loop
          for j in GetOrgnHierC(i.forusor_orgn_code, l_user_code) loop
              select ftvacci_acci_code
                 into acci_code
                 from ftvacci
                 where ftvacci_orgn_code = j.ftvorgn_orgn_code
                 and ftvacci_coas_code = '1'
                 and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
                 and ftvacci_status_ind = 'A'
                 and trunc(ftvacci_term_date) > trunc(SYSDATE)
                 and ftvacci_acci_code like p_Index;
          p_list.extend;       
          p_list(tab_indx) :=  FinIndxTabType(acci_code);
          tab_indx := tab_indx + 1;
        end loop;
     end loop;
  end if;   
exception 
when others then
dbms_output.put_line(stmt_id);
dbms_output.put_line(sqlerrm);
end GET_SECURITY_INDEX_LIST;


   

--function F_SECURITY_INDEX_LIST(p_uni  in varchar2,
--                                                    p_Index in varchar2) return FININDXTAB
-- is                                                   
function F_SECURITY_INDEX_LIST(p_uni  in varchar2,
                               p_Index in varchar2) return FININDXTAB PIPELINED
 is                                                   

--p_list FININDXTAB  := FININDXTAB();
p_list FinIndxTabType  := FinIndxTabType(null);                                                
                                                   

tab_indx        number := 1;
l_user_code     gobeacc.gobeacc_username%TYPE;
has_master_lvl  varchar2(1) := 'N';
acci_code       varchar2(6);
stmt_id         varchar2(100);

CURSOR GetAll is
     select ftvacci_acci_code 
      from ftvacci
      where ftvacci_coas_code = '1'
      and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
      and ftvacci_status_ind = 'A'
      and nvl(trunc(ftvacci_term_date), trunc(sysdate+1)) > trunc(SYSDATE)
      and ftvacci_acci_code like p_Index;

CURSOR GetFundHierC(fund_code_in varchar2, l_user_code varchar2) IS
      SELECT FTVFUND_FUND_CODE
        FROM FTVFUND
       WHERE FTVFUND_FUND_CODE IN  (
         SELECT FORUSFN_FUND_CODE
           FROM FORUSFN
          WHERE FORUSFN_USER_ID_ENTERED = l_user_code
            AND FORUSFN_COAS_CODE = '1'
            AND FORUSFN_FUND_CODE IS NOT NULL )
         CONNECT BY FTVFUND_FUND_CODE = PRIOR FTVFUND_FUND_CODE_PRED
                AND  FTVFUND_COAS_CODE = PRIOR FTVFUND_COAS_CODE
                AND  FTVFUND_EFF_DATE <= SYSDATE
                AND  FTVFUND_NCHG_DATE > SYSDATE
              START WITH FTVFUND_FUND_CODE = fund_code_in
                AND  FTVFUND_COAS_CODE = '1'
                AND  FTVFUND_EFF_DATE <= SYSDATE
                AND  FTVFUND_NCHG_DATE > SYSDATE ;
                
   CURSOR GetFundC IS
      SELECT FORUSFN_FUND_CODE
        FROM FORUSFN
      WHERE FORUSFN_COAS_CODE = '1'
         AND FORUSFN_USER_ID_ENTERED = l_user_code ;
         
    CURSOR GetOrgnHierC(orgn_code_in varchar2, l_user_code varchar2) IS
      SELECT FTVORGN_ORGN_CODE
        FROM FTVORGN
       WHERE FTVORGN_ORGN_CODE IN  (
         SELECT FORUSOR_ORGN_CODE
           FROM FORUSOR
          WHERE FORUSOR_USER_ID_ENTERED = l_user_code
            AND FORUSOR_COAS_CODE = '1')
         CONNECT BY FTVORGN_ORGN_CODE = PRIOR FTVORGN_ORGN_CODE_PRED
                      AND  FTVORGN_COAS_CODE = PRIOR FTVORGN_COAS_CODE
                      AND  FTVORGN_EFF_DATE <= SYSDATE
                      AND  FTVORGN_NCHG_DATE > SYSDATE
                    START WITH FTVORGN_ORGN_CODE = orgn_code_in
                      AND  FTVORGN_COAS_CODE = '1'
                      AND  FTVORGN_EFF_DATE <= SYSDATE
                      AND  FTVORGN_NCHG_DATE > SYSDATE ;
 
 CURSOR GetOrgnC IS
      SELECT FORUSOR_ORGN_CODE
        FROM FORUSOR
       WHERE FORUSOR_COAS_CODE = '1'
         AND FORUSOR_USER_ID_ENTERED = l_user_code ;
begin

  -- p_list := FININDXTAB();
   
  /* get the username for the uni passed in */
  stmt_id := 'Get pidm';
  select gobeacc_username
    into l_user_code
    from spriden, gobeacc
    where spriden_pidm = gobeacc_pidm 
    and spriden_ntyp_code = 'UNI'
    and spriden_id = p_uni;

  /* check for master level access */
  stmt_id := 'Check master level access';
  begin 
     select 'Y'
      into has_master_lvl
      FROM FOBPROF
       WHERE FOBPROF_USER_ID = l_user_code
       AND (fobprof_master_fund_ind is not null or fobprof_master_orgn_ind is not null);
       
  exception
     when no_data_found then has_master_lvl := 'N';
     when others then dbms_output.put_line(sqlerrm);
  end;
  
  /* if the user has master level acces, return all indexes */
  stmt_id := 'Selecting indexes.  Master lvl is '||has_master_lvl;
  if has_master_lvl = 'Y' then
/*     for i in GetAll loop
       p_list.extend;
       p_list(tab_indx) := FinIndxTabType(i.ftvacci_acci_code);
       tab_indx := tab_indx + 1;
*/
     open GetAll;
        loop
        fetch GetAll into p_list.index_code;
        exit when GetAll%NOTFOUND;
        PIPE ROW(p_list);
      end loop;
      close GetAll;
  else
      for i in GetFundC loop
          for j in GetFundHierC(i.forusfn_fund_code, l_user_code) loop
               select ftvacci_acci_code
                 into acci_code
                 from ftvacci
                 where ftvacci_fund_code = j.ftvfund_fund_code
                 and ftvacci_coas_code = '1'
                 and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
                 and ftvacci_status_ind = 'A'
                 and trunc(ftvacci_term_date) > trunc(SYSDATE)
                 and ftvacci_acci_code like p_Index;
          /*p_list.extend;
          p_list(tab_indx) :=  FinIndxTabType(acci_code);
          tab_indx := tab_indx + 1;
          */
          p_list.index_code := acci_code;
          PIPE ROW(p_list);          
        end loop;
     end loop;
 stmt_id := 'Selecting Orgs';
    for i in GetOrgnC loop
          for j in GetOrgnHierC(i.forusor_orgn_code, l_user_code) loop
              select ftvacci_acci_code
                 into acci_code
                 from ftvacci
                 where ftvacci_acci_code = j.ftvorgn_orgn_code
                 and ftvacci_coas_code = '1'
                 and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
                 and ftvacci_status_ind = 'A'
                 and trunc(ftvacci_term_date) > trunc(SYSDATE)
                 and ftvacci_acci_code like p_Index;
     /*     p_list.extend;       
          p_list(tab_indx) :=  FinIndxTabType(acci_code);
          tab_indx := tab_indx + 1;
      */
          p_list.index_code := acci_code;
          PIPE ROW(p_list);
        end loop;
     end loop;
  end if;   
  --return p_list;
exception 
when others then
dbms_output.put_line(stmt_id);
dbms_output.put_line(sqlerrm);
end F_SECURITY_INDEX_LIST;

procedure populate_fzrbxfr is

cursor all_uni is
select distinct spriden_id
from fobprof, spriden, pebempl, gobeacc
where fobprof_user_id = gobeacc_username
and spriden_pidm = gobeacc_pidm 
and spriden_pidm = pebempl_pidm
and pebempl_empl_status <> 'T'
and spriden_ntyp_code = 'UNI'
and (fobprof_master_fund_ind is not null or fobprof_master_orgn_ind is not null)
union
select distinct spriden_id
from forusor, spriden, pebempl, gobeacc
where forusor_user_id_entered = gobeacc_username
and spriden_pidm = gobeacc_pidm
and spriden_pidm = pebempl_pidm
and pebempl_empl_status <> 'T'
and spriden_ntyp_code = 'UNI'
and forusor_coas_code = '1'
union
select distinct spriden_id
from FIMSMGR.FORUSFN, spriden, pebempl, gobeacc
where forusfn_user_id_entered = gobeacc_username
and spriden_pidm = gobeacc_pidm
and spriden_pidm = pebempl_pidm
and pebempl_empl_status <> 'T'
and spriden_ntyp_code = 'UNI'
and forusfn_coas_code = '1'
and forusfn_fund_code <> '1'
union
select distinct spriden_id
from fimsmgr.fzrappr, spriden, pebempl, gobeacc
where spriden_pidm = fzrappr_pidm
and spriden_pidm = gobeacc_pidm
and spriden_pidm = pebempl_pidm
and pebempl_empl_status <> 'T'
and spriden_ntyp_code = 'UNI'
and fzrappr_system in ('REPORT','AUTHSIG')
AND fzrappr_sys_role in ('BUDADM','RESPPERS','AUTHSIG')
AND trunc(fzrappr_exp_date) >= trunc(sysdate)
;
                                                   
cursor all_budofc is
select distinct spriden_id
from fzrappr, spriden
where fzrappr_pidm = spriden_pidm 
and fzrappr_system = 'REPORT'
and fzrappr_sys_role = 'BUDOFC'
and spriden_ntyp_code = 'UNI'
AND trunc(fzrappr_exp_date) >= trunc(sysdate);

l_user_code     gobeacc.gobeacc_username%TYPE;
has_master_lvl  varchar2(1) := 'N';
acci_code       varchar2(6);
stmt_id         varchar2(100);

CURSOR GetAll is
     select ftvacci_acci_code 
      from ftvacci
      where ftvacci_coas_code = '1'
      and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
      and ftvacci_status_ind = 'A'
      and nvl(trunc(ftvacci_term_date), trunc(sysdate+1)) > trunc(SYSDATE);

CURSOR GetFundHierC(fund_code_in varchar2, l_user_code varchar2) IS
      SELECT FTVFUND_FUND_CODE
        FROM FTVFUND
       WHERE FTVFUND_FUND_CODE IN  (
         SELECT FORUSFN_FUND_CODE
           FROM FORUSFN
          WHERE FORUSFN_USER_ID_ENTERED = l_user_code
            AND FORUSFN_COAS_CODE = '1'
            AND FORUSFN_FUND_CODE IS NOT NULL )
         CONNECT BY FTVFUND_FUND_CODE = PRIOR FTVFUND_FUND_CODE_PRED
                AND  FTVFUND_COAS_CODE = PRIOR FTVFUND_COAS_CODE
                AND  FTVFUND_EFF_DATE <= SYSDATE
                AND  FTVFUND_NCHG_DATE > SYSDATE
              START WITH FTVFUND_FUND_CODE = fund_code_in
                AND  FTVFUND_COAS_CODE = '1'
                AND  FTVFUND_EFF_DATE <= SYSDATE
                AND  FTVFUND_NCHG_DATE > SYSDATE ;
                
   CURSOR GetFundC IS
      SELECT FORUSFN_FUND_CODE
        FROM FORUSFN
      WHERE FORUSFN_COAS_CODE = '1'
         AND FORUSFN_USER_ID_ENTERED = l_user_code 
         and forusfn_fund_code <> '1';
         
    CURSOR GetOrgnHierC(orgn_code_in varchar2, l_user_code varchar2) IS
      SELECT FTVORGN_ORGN_CODE
        FROM FTVORGN
       WHERE FTVORGN_ORGN_CODE IN  (
         SELECT FORUSOR_ORGN_CODE
           FROM FORUSOR
          WHERE FORUSOR_USER_ID_ENTERED = l_user_code
            AND FORUSOR_COAS_CODE = '1')
         CONNECT BY FTVORGN_ORGN_CODE = PRIOR FTVORGN_ORGN_CODE_PRED
                      AND  FTVORGN_COAS_CODE = PRIOR FTVORGN_COAS_CODE
                      AND  FTVORGN_EFF_DATE <= SYSDATE
                      AND  FTVORGN_NCHG_DATE > SYSDATE
                    START WITH FTVORGN_ORGN_CODE = orgn_code_in
                      AND  FTVORGN_COAS_CODE = '1'
                      AND  FTVORGN_EFF_DATE <= SYSDATE
                      AND  FTVORGN_NCHG_DATE > SYSDATE ;
 
 CURSOR GetOrgnC IS
      SELECT FORUSOR_ORGN_CODE
        FROM FORUSOR
       WHERE FORUSOR_COAS_CODE = '1'
         AND FORUSOR_USER_ID_ENTERED = l_user_code ;
         
 CURSOR GetFzrappr(p_spriden_id varchar2)is
     SELECT fzrappr_index
       FROM fzrappr, spriden
      WHERE fzrappr_system in ('REPORT','AUTHSIG')
        AND fzrappr_sys_role in ('BUDADM','RESPPERS','AUTHSIG')
        AND trunc(fzrappr_exp_date) >= trunc(sysdate)
        AND fzrappr_pidm = spriden_pidm
        AND spriden_ntyp_code = 'UNI'
        and spriden_id = p_spriden_id
        AND not exists (select 'y' from fzrbxfr where fzrbxfr_spriden_id = p_spriden_id and fzrbxfr_acci_code = fzrappr_index);
begin

   delete from fzrbxfr;
   
for t in all_uni loop
  /* get the username for the uni passed in */
  
  l_user_code := null;
  has_master_lvl := null;
  
  stmt_id := 'Get pidm';
  select gobeacc_username
    into l_user_code
    from spriden, gobeacc
    where spriden_pidm = gobeacc_pidm 
    and spriden_ntyp_code = 'UNI'
    and spriden_id = t.spriden_id;

  /* check for master level access */
  /* 10/2/2017 - remove master level access */
 -- stmt_id := 'Check master level access';
 -- begin 
 --    select 'Y'
 --     into has_master_lvl
 --     FROM FOBPROF
 --      WHERE FOBPROF_USER_ID = l_user_code
 --      AND (fobprof_master_fund_ind is not null or fobprof_master_orgn_ind is not null);
       
 -- exception
 --    when no_data_found then has_master_lvl := 'N';
 --    when others then dbms_output.put_line(sqlerrm);
 -- end;
  
  /* if the user has master level acces, return all indexes */
--  stmt_id := 'Selecting indexes.  Master lvl is '||has_master_lvl;
--  if has_master_lvl = 'Y' then
--    insert into fzrbxfr
--     select t.spriden_id, ftvacci_acci_code 
--      from ftvacci
--      where ftvacci_coas_code = '1'
--      and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
--      and ftvacci_status_ind = 'A'
--      and nvl(trunc(ftvacci_term_date), trunc(sysdate+1)) > trunc(SYSDATE)
--      ;
--  else
  stmt_id := 'Getting Funds for '||t.spriden_id;
      for i in GetFundC loop
          exit when GetFundC%NOTFOUND;
          for j in GetFundHierC(i.forusfn_fund_code, l_user_code) loop
          exit when GetFundHierC%NOTFOUND;
              begin
               select ftvacci_acci_code
                 into acci_code
                 from ftvacci
                 where ftvacci_fund_code = j.ftvfund_fund_code
                 and ftvacci_coas_code = '1'
                 and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
                 and ftvacci_status_ind = 'A'
                 and trunc(nvl(ftvacci_term_date, sysdate+1)) > trunc(SYSDATE);
        stmt_id := 'insert to fzrbxfr '||t.spriden_id||' '||acci_code;
         insert into fzrbxfr values(t.spriden_id, acci_code);
         exception
         when others then null;
         end;
        end loop;     
     end loop;
     
     
  stmt_id := 'Getting Orgs for '||t.spriden_id;
  
    for x in GetOrgnC loop
           exit when GetOrgnC%NOTFOUND;
           
          for y in GetOrgnHierC(x.forusor_orgn_code, l_user_code) loop
           exit when GetOrgnHierC%NOTFOUND;

             begin
              select ftvacci_acci_code
                 into acci_code
                 from ftvacci
                -- where ftvacci_orgn_code = y.ftvorgn_orgn_code
                 where ftvacci_acci_code = y.ftvorgn_orgn_code
                 and ftvacci_coas_code = '1'
                 and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
                 and ftvacci_status_ind = 'A'
                 and trunc(nvl(ftvacci_term_date, sysdate+1)) > trunc(SYSDATE);         
          
          insert into fzrbxfr values(t.spriden_id, acci_code);
          exception
           when others then null;
          end;
        end loop;
     end loop;
 -- end if;   
 
    stmt_id := 'Getting fzrappr indexes for '||t.spriden_id;
    
     for z in GetFzrappr(t.spriden_id) loop
           exit when GetFzrappr%NOTFOUND;     
          insert into fzrbxfr values(t.spriden_id, z.fzrappr_index);
     end loop;

end loop;   -- all_uni

  stmt_id := 'Getting additional indexes for Budget Office';
  
  for s in all_budofc loop
     exit when all_budofc%NOTFOUND;
     insert into fzrbxfr 
     select distinct s.spriden_id, fgbtrnd_acci_code
      from fgbtrnd
     where fgbtrnd_fsyr_code between'15' and '18'
       and fgbtrnd_rucl_code in ('BD02','BD04', 'BD05')
       and fgbtrnd_ledger_ind = 'G'
       and fgbtrnd_coas_code = '1'
       and fgbtrnd_doc_code like 'J%'
       and fgbtrnd_user_id in (
                  select gobeacc_username from fzrappr, gobeacc
                  where fzrappr_sys_role = 'BUDOFC'
                   and gobeacc_pidm = fzrappr_pidm)
       and not exists (select 'y' from fzrbxfr b where b.fzrbxfr_spriden_id = s.spriden_id and b.fzrbxfr_acci_code = fgbtrnd_acci_code);
 end loop;
exception 
when others then
dbms_output.put_line(stmt_id);
dbms_output.put_line(sqlerrm);
end populate_fzrbxfr;
  
end bzebxfr;
/
