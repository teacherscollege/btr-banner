   CREATE OR REPLACE VIEW "BANINST1"."FZVRPBA" ("FZVRPBA_INDEX", "FZVRPBA_TITLE", "FZVRPBA_USERNAME", "FZVRPBA_UNI", "FZVRPBA_PIDM", "FZVRPBA_ROLETITLE", "FZVRPBA_ROLELEVEL", "FZVRPBA_EFF_DATE", "FZVRPBA_EXP_DATE", "FZVRPBA_INDEX_NCHG_DATE") AS 
  select fzrappr_index as fzvrpba_index,
ftvacci_title as fzvrpba_title, 
a.spriden_first_name||' '||a.spriden_last_name as fzvrpba_UserName,  
uni.spriden_id as fzvrpba_UNI,
a.spriden_pidm as fzvrpba_PIDM, 
fzrappr_sys_role as fzvrpba_RoleTitle,
case fzrappr_sys_role when 'RESPPERS' then '1'
                    when 'BUDADM'  then '2'
                    else '3'
                    end as fzvrpba_RoleLevel,
fzrappr_eff_date,
fzrappr_exp_date,
ftvacci_nchg_date
from fzrappr, spriden a, ftvacci, (select spriden_pidm, spriden_id from spriden
                                                    where spriden_ntyp_code = 'UNI') UNI
where fzrappr_pidm = a.spriden_pidm
and a.spriden_change_ind is null
and a.spriden_pidm = uni.spriden_pidm
and fzrappr_index = ftvacci_acci_code
and fzrappr_fund_code = ftvacci_fund_code
and trunc(ftvacci_nchg_date) = trunc(to_date('31-dec-2099','dd-mon-yyyy'))
and trunc(fzrappr_exp_date) = trunc(to_date('31-dec-2099','dd-mon-yyyy'))
and fzrappr_system = 'REPORT';

create public synonym fzvrpba for fzvrpba;

grant select on fzvrpba to public;
